// Logic for authorization via tokens and create function to create access token
const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// jwt.sign(payload, secretKey, option)
	return jwt.sign(data, secret, {});
};

// Logic to verify and decode the user information
// Next passes on the request to the next middleware function/route/request handler in the stack
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if (typeof token !== 'undefined') {
		// used to get the token only
		token = token.slice(7, token.length);
		
		return jwt.verify(token, secret, (err, data) => {
			// next() passes the request to the next callback function in the route
			return (err) ? res.send({auth: 'failed'}) : next()
			});
	} else {
		return res.send({auth: 'failed'});
	}
};

module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			// {complete: true}, grabs both the request header and payload
			return (err) ? null : jwt.decode(token, {complete: true}).payload
			});
	} else {
		return null;
	}
};
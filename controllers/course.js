// CONTROLLER - Course
const Course = require('../model/Course');

// Logic
// Creating a new course
module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
		});
	
	return course.save().then((course, err) => {
		return (err) ? false : true;
	});
};

// To retrieve all active courses
module.exports.getAll = () => {
	return Course.find({isActive: true})
	.then(courses => {
		return courses;
	});
};

// To retrieve a specific course
module.exports.get = (params) => {
	return Course.findById(params.courseId)
	.then(courses => {
		return courses;
	});
};

// To update a course
module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	};
	
	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// To soft delete (archive) a course
module.exports.archive = (params) => {
	const archives = {
		isActive: false
	};
	return Course.findByIdAndUpdate(params.courseId, archives).then((softD, err) => {
		return (err) ? false : true;
	});
};
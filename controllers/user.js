// CONTROLLER - User
const User = require('../model/User');
const Course = require('../model/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Logic
// Check if an email already exists
module.exports.emailExists = (params) => {
	return User.find({email: params.email})
	.then(result => {
		return result.length > 0 ? true : false
	});	
};

// Registering a new user
module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNum: params.mobileNum,
		password: bcrypt.hashSync(params.password, 10),
	});
	
	return user.save().then((user, err) => {
		return (err) ? false : true;
	});
};

// Authentication logic when user is logging in
module.exports.login = (params) => {
	// Checks if there is an email in the database
	return User.findOne({ email: params.email })
	.then(user => {
		if (user === null) {
			return `No such email found.`;
		}
		
		// Compare the password received from the user to the hashed password, return true if values matched
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password);
		
		// The mongoose toObject method converts the mongoose into plain JS object
		// Used to show an object representation of mongoose mode
		// Mongoose object will have access to .save() method while plain JS won't
		// Code snippet below creates a unique token
		if (isPasswordMatched) {
			return {accessToken: auth.createAccessToken(user.toObject())};
		} else {
			return `Incorrect password, please try again.`;
		}
	});
};

// Create the controller logic for getting the user information
module.exports.get = (params) => {
	return User.findById(params.userId)
	.then(user => {
		// re-assign the password to undefined so it won't be retrieved along with other user data
		user.password = undefined;
		return user;
	});
};

// Create logic for when a user is enrolled in a course
module.exports.enroll = (params) => {
	return User.findById(params.userId)
	.then(user => {
		user.enrollments.push({courseId: params.courseId});
		
	return user.save().then((user, err) => {
		return Course.findById(params.courseId)
		.then(course => {
			course.enrollees.push({userId: params.userId});
		});
			
		return course.save().then((course,err) => {
			return (err) ? 'Enrollment unsuccessful, try again.' : 'Enrollment successful!';
		});
	});
	});
};

// Miscellaneous Controllers 
module.exports.updateDetails = (params) => {
	
};

module.exports.changePassword = (params) => {
	
};
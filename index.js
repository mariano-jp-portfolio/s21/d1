// Declaring Express.js
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Routes
const userRoutes = require('./routes/user');
app.use('/app/users', userRoutes);

const courseRoutes = require('./routes/course');
app.use('/app/courses', courseRoutes);

// .env - is used to hide various processes
require('dotenv').config();

// Mongoose Connection
const connectionString = process.env.DB_CONNECTION_STRING;
mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
// Checking the connection
mongoose.connection.once('open', () => {console.log("Database Connection Successful!")});

// Listening Port
const port = process.env.PORT;
app.listen(port, () => {
    console.log(`Listening on port ${port}.`);
});
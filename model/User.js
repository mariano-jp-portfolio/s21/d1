// MODEL - User
const mongoose = require('mongoose');

// User Schema
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	mobileNum: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required."]
			},
		
		
			enrolledOn: {
				type: Date,
				default: new Date()
			},
		
		
			status: {
				type: String,
				default: "Enrolled" //alt values can be cancelled or completed
			}
		}
	]
});

// Export
module.exports = mongoose.model('User', userSchema);
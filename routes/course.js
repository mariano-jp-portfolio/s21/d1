// ROUTER - Course
const express = require('express');

// Create a new router object
const router = express.Router();
const CourseController = require('../controllers/course');
const auth = require('../auth');

// HTTP Methods
// Route to create a new course
router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body)
	.then(result => res.send(result));
});

// Route to retrieve available courses
router.get('/', (req, res) => {
	CourseController.getAll()
	.then(courses => {
		res.send(courses);
	});
});

// Route to retrieve details of a specific course
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId;
	CourseController.get({courseId})
	.then(course => {
		res.send(course);
	});
});

// Route for updating a course
router.put('/', auth.verify, (req, res) => {
	CourseController.update(req.body)
	.then(result => {
		return res.send(result);
	});
});

// Route for soft deletion (archiving) of a course
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.archive({courseId})
	.then(result => {
		res.send(result);
	});
});

// Export
module.exports = router;
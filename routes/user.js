// ROUTER - User
const express = require('express');

// Create a new router object
const router = express.Router();
const UserController = require('../controllers/user');
// To create a route for getting the details of a user
const auth = require('../auth');

// HTTP Methods
// Route to check if the email already exists
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body)
	.then(result => res.send(result));
});

// Route to register a user
router.post('/', (req, res) => {
	UserController.register(req.body)
	.then(result => res.send(result));
});

// Route to log in a user
router.post('/login', (req, res) => {
	UserController.login(req.body)
	.then(result => res.send(result));
});

// Route for getting a user's details
// Add logic to .verify
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	UserController.get({userId: user.id})
	.then(user => res.send(user));
});

// Route for enrolling a user
router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};
	UserController.enroll(params).then(result => res.send(result));
});

// Miscellaneous Routes
router.put('/details', (req, res) => {
	UserController.updateDetails();
});

router.put('/change-password', (req, res) => {
	UserController.changePassword();
});

// Export
module.exports = router;